$(document).ready(function () {
	$(window).load(function () {
		var book_offset = $("#booknow").offset().top - 132;
		var welcome = $("#booknow").offset().top - 500;
		//alert($("body").find("#welcome_text").length)
		if( $("body").find("#welcome_text").length >= 1)
		{
			var offer = $("#offer").offset().top-500;
			var location = $("#our-location").offset().top-500;
			var the_say = $("#thesay").offset().top-700;
			
		}
		
		$(window).scroll(function () {
			var offset = $(this).scrollTop();
			
			if (offset >= book_offset) {
				$("#booknow").addClass("float");
			} else {
				$("#booknow").removeClass("float");
			}
			/*scroll animation*/

			
			if (offset >= welcome) {
				if (!TweenMax.isTweening($(".animated")))									
				$("#welcome_text").animate_home();						
			}			
			if (offset >= offer) {
				if (!TweenMax.isTweening($(".animated"),$(".list-animated")))
				$("#offer").animate_home();
				
			}
			if (offset >= location) {
				if (!TweenMax.isTweening($(".animated")))
				$("#our-location").animate_home();
			}
			if (offset >= the_say) {
				//if (!TweenMax.isTweening($(".animated")))
				$("#thesay").animate_home();
			}

			

		});
	});
	$("#menu-mobile").click(function () {
		if ($("nav.main-menu").is(":hidden")) {
			$("nav.main-menu").show()
			TweenMax.to($("nav.main-menu"), 0.3, {
				rotationX : 0,
				transformOrigin : 'center top',
				transformPerspective : 1600,
				opacity : 1,
				perspective : 1500,
				delay : 0.2,
				ease : Sine.easeIn
			})
		} else {
			TweenMax.to($("nav.main-menu"), 0.3, {
				rotationX : '-90',
				transformOrigin : 'top',
				transformPerspective : 0,
				opacity : 0,
				display : 'none',
			})
		}
	});
	$(".main-menu .arrow-up a").click(function(){
		TweenMax.to($("nav.main-menu"), 0.3, {
				rotationX : '-90',
				transformOrigin : 'top',
				transformPerspective : 0,
				opacity : 0,
				display : 'none',
		})
	});
	$(".search_mobile").click(function () {
		if ($("#search-mobile").is(":hidden")) {
			$("#search-mobile").show()
			TweenMax.to($("#search-mobile"), 0.3, {
				rotationX : 0,
				transformOrigin : 'center top',
				transformPerspective : 1600,
				opacity : 1,
				perspective : 1500,
				delay : 0.2,
				ease : Sine.easeIn
			})
		} else {
			TweenMax.to($("#search-mobile"), 0.3, {
				rotationX : '-90',
				transformOrigin : 'top',
				transformPerspective : 0,
				opacity : 0,
				display : 'none',
			})
		}
	});
	$("#book-now").click(function () {
		if ($("#booknow").is(":hidden")) {
			$("#booknow").show()
			TweenMax.to($("#booknow"), 0.3, {
				rotationX : 0,
				transformOrigin : 'center top',
				transformPerspective : 1600,
				opacity : 1,
				perspective : 1500,
				delay : 0.2,
				ease : Sine.easeIn
			})
		} else {
			TweenMax.to($("#booknow"), 0.3, {
				rotationX : '-90',
				transformOrigin : 'top',
				transformPerspective : 0,
				opacity : 0,
				display : 'none',
			})
		}
	});
	$("#booknow .arrow-up a").click(function(){
		TweenMax.to($("#booknow"), 0.3, {
				rotationX : '-90',
				transformOrigin : 'top',
				transformPerspective : 0,
				opacity : 0,
				display : 'none',
		})
	});
	$(".data_table").each(function () {
		$(this).wrap("<div class='overflow_table'></div>")
	})
	$(".language div").text($(".language select option:first").text());
	$(".language select").on('change', function () {
		var label = $(this).children("option:selected").text();
		$(this).siblings("div").text(label);
	});
	$(".datepicker").datepicker();
	$(".select_custom").each(function (index) {
		var parent = $(this)
			$(this).append("<span></span> <input name='' type='hidden' value='' id='select_" + index + "'>");
		var val_default = $(this).children("ul").children("li:first").text();
		$(this).children("span").text(val_default);
		$(this).click(function (e) {
			$(this).find("ul").slideToggle(400);
		});
		$(this).find("ul").children("li").click(function (e) {
			parent.children("span").text($(this).text());
			parent.find("input[type=hidden]").val($(this).text());
		});
	});
	$.fn.listImage = function () {
		var parent = $(this);
		var child = $(this).children("li");
		var video = $(this).children("li.video");
		TweenMax.lagSmoothing(1000, 16);
			child.each(function (index) {
				$(this).find(".images").append("<h4>" + $(this).find("img").attr("alt") + "</h4>");
				var ua = window.navigator.userAgent;
				var msie = ua.indexOf("MSIE ");
				if (msie > 0) {
					$(this).find(".images").append("<div class='grey'></grey>");
				}
				$(this).find("h4").css({
					width : parseInt($(this).find("h4").width() + 1) + "px",
					left : 0,
					right : 0
				})
				var href = video.attr("data-url");
				video.children(".images").eq(index).append("<a href='" + href + "' class='icon_play fancybox-media'></a>");
			});
		child.find(".images").hover(function () {
			TweenMax.to($(this).find("img"), 0.2, {
				scale : 1.1
			});
			/*TweenMax.to($(this).find(".icon_play"), 0.5, {
				scale : 1.0,
				ease : Cubic.easeIn
			});*/
			if(parent.is(".media"))
			{
				TweenMax.to($(this).find("h4"), 0.5, {
					bottom : '30px',					
				});
			}
		}, function () {
			TweenMax.to($(this).find("img"), 0.5, {
				scale : 1.0
			});
			/*TweenMax.to($(this).find(".icon_play"), 0.5, {
				scale : 0.0
			});*/
			if(parent.is(".media"))
			{
				TweenMax.to($(this).find("h4"), 0.5, {
					bottom : '-30px',					
				});
			}
		})
	}
	$(".list-image").listImage();
	$(".fm_select").each(function (index, element) {
		var width = $(this).outerWidth();
		var value = $(this).children('option:first').text();
		$(this).wrap("<div class=selector_form>");
		$(".selector_form").eq(index).css('width', width + 'px').prepend("<span>" + value + "</span>");
		$(this).on('change', function () {
			var value = $(this).children(":selected").text();
			if (value.length >= 20) {
				$(this).siblings("span").text(value.substring(0, 20) + "..");
			} else {
				$(this).siblings("span").text(value);
			}
		});
	});
	$(".select-room li").find(".css-label").click(function () {
		$(".select-room li").removeClass("checked");
		$(this).parent().parent().parent().addClass("checked");
	});
	$("#subscribe").submit(function () {
		$.fancybox.open([{
					href : '#thanks'
				}
			]);
		return false;
	});
	/*animate list room*/
	$(".list-room li").each(function(index){
		$(this).find(".box").wrapInner("<div class='outerWrap'><div class='innerWrap'></div><div>");
		var hi = $(this).find(".innerWrap").outerHeight();
		$(".outerWrap").css({
			'position' : 'relative',
			height : '100%',			
		});
		$(this).find(".innerWrap").css({
			height : hi+'px',
			bottom : 0,
			top : 0,
			margin : 'auto',
			position : 'absolute',
			width : '100%'
		});
		
	});
	
	$(".list-room").children("li").hover(function(){
		TweenMax.to($(this).find("img"), 0.2, {
				scale : 1.1
			});
	},function(){
		TweenMax.to($(this).find("img"), 0.2, {
				scale : 1.0
			});
	})
	/*animate home*/
	$.fn.animate_home = function () {
		var $this = $(this);
		var title = $(this).find("h1.animated");
		var title2 = $(this).find("h2.animated");
		var p_anim = $(this).find("p.animated");
		var link_arrow = $(this).find(".link-arrow.animated");
		title.css({
			'transform' : 'scale(0.0)',
		});
		title2.css({
			'transform' : 'scale(0.0)',
		});
		p_anim.css({
			position : 'relative',
			top : '50px'
		})
		link_arrow.css({
			right : '50px'
		})
		$(this).find(".list-animated li").css({
			marginTop : '100px'
		})		
		TweenMax.allTo([title, title2], 1, {
			scale : 1.0,
			opacity : 1,
			repeat :0,
			yoyo : true,
			onComplete : function () {
				title.removeClass("animated").removeAttr("style");
				title2.removeClass("animated").removeAttr("style");	
				this.pause();			
			}
		});
		TweenMax.to(p_anim, 1, {
			opacity : 1,
			top : '0px',
			delay : 0.7,
			repeat :0,
			yoyo : true,
			onComplete : function () {
				p_anim.removeClass("animated").removeAttr("style");
			}
		});
		TweenMax.to(link_arrow, 1, {
			opacity : 1,
			right : '0px',
			delay : 0.7,
			yoyo : true,
			onComplete : function () {
				link_arrow.removeClass("animated").removeAttr("style");
			}
		});
		TweenMax.staggerTo($(this).find(".list-animated li"), 1.3, {
			opacity : 1,
			marginTop : '0px',
			delay : 1,
			yoyo : true,
			onComplete : function () {
				$this.find(".list-animated").removeClass("list-animated");
			}
		}, 0.3);
		TweenMax.staggerTo($(this).find(".column"), 1.3, {
			opacity : 1,
			delay : 1,
			yoyo : true,
			onComplete : function () {
				$(this).find(".column").removeClass("list-animated");
			}
		}, 0.3);
	}

});
