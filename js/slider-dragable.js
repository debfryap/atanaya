$.fn.slider_drag = function () {
	var parent = $(this)
		var box = $(this).children(".box");
	var qty_box = box.length;
	$(this).append("<nav></nav>");
	$(this).append("<a href='#' class='arrow prev'>prev</a><a href='#' class='arrow next'>next</a> <div class='icon-drag'></div>")
	box.each(function (index, element) {
		var w_image = $(this).find("img").attr("width");
		var h_image = $(this).find("img").attr("height");
		box.eq(index).css({
			zIndex : qty_box - index,
			opacity : 0,
		});
		if (w_image != undefined) {
			box.eq(index).css({
				width : w_image,
				height : h_image,
			});
		} else {
			box.eq(index).css({
				width : 960,
				height : 510,
			})
		}
		box.eq(index).attr("id", "box_" + index);
		parent.children("nav").append("<a href='#box_" + index + "'>" + index + "</a>");
	});
	box.first().css({
		opacity : 1,
	});
	var nav = parent.children("nav").children("a");
	nav.first().addClass("active");
	var arrow = parent.children(".arrow");
	nav.click(function () {
		var target = $(this).attr("href");
		var lastIndex = target.substring(target.length - 1, target.length);
		var prev = parseInt(lastIndex - 1);
		var next = parseInt(lastIndex) + 1;
		if (prev < 0) {
			prev = 0;
		}
		if (next > nav.length - 1) {
			next = nav.length - 1;
		}
		parent.children(".prev").attr("href", "#box_" + prev);
		parent.children(".next").attr("href", "#box_" + next);
		nav.removeClass("active");
		$(this).addClass("active");
		TweenMax.to(box, 1, {
			opacity : 0,
			zIndex : 1,
		})
		TweenMax.to(target, 1, {
			opacity : 1,
			zIndex : 1000,
		})
		return false;
	});
	parent.children(".prev").attr("href", "#box_0");
	parent.children(".next").attr("href", "#box_1");
	arrow.click(function () {
		var target = $(this).attr("href");
		var lastIndex = target.substring(target.length - 1, target.length);
		var prev = parseInt(lastIndex - 1);
		var next = parseInt(lastIndex) + 1;
		if (prev < 0) {
			prev = 0;
		}
		if (next > nav.length - 1) {
			next = nav.length - 1;
		}
		parent.children(".prev").attr("href", "#box_" + prev);
		parent.children(".next").attr("href", "#box_" + next);
		nav.removeClass("active");
		parent.children("nav").children("a[href=" + target + "]").addClass("active");
		TweenMax.to(box, 1, {
			opacity : 0,
			zIndex : 1,
		})
		TweenMax.to(target, 1, {
			opacity : 1,
			zIndex : 1000,
		})
		return false;
	});
	Draggable.create(".box", {
		type : "top,left",
		edgeResistance : 0.75,
		bounds : "#dragable",
		throwProps : true
	});
}
