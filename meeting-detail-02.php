<?php $page = "page5"; ?>
<?php include('inc_header.php');?>
<!-- middle -->
<div id="banner-content"><img src="images/slider/banner-offer.jpg" alt=""></div>
<?php include("inc_booking.php"); ?>
<div id="breadcumb">
  <div class="wrapper"><a href="#">Home</a> / <a href="#">Meeting</a></div>
</div>
<section id="main-content" class="with-flower">
  <div class="wrapper">
    <div class="title-detail">Meeting Room Request</div>
    <p>To begin planning your special event or business meeting at The Atanaya Hotel in Bali today, please fill out all of the necessary information in the form below. A member of our dedicated staff will contact you in a timely manner.</p>
    <div class="double_line"></div>
    <form action="meeting-detail-03.php" method="get" class="form_style afterclear">
      <h5>Request for Proposal</h5>
      <div class="left">
        <div>
          <label>Name of Company/Private <span>*</span></label>
          <input name="" type="text">
        </div>
        <div>
          <label>Address <span>*</span></label>
          <input name="" type="text">
        </div>
        <div>
          <label>Contact Person <span>*</span></label>
          <input name="" type="text">
        </div>
      </div>
      <div class="right">
        <div>
          <label>Phone <span>*</span></label>
          <input name="" type="text">
        </div>
        <div>
          <label>Fax </label>
          <input name="" type="text">
        </div>
        <div>
          <label>Email</label>
          <input name="" type="text">
        </div>
      </div>
      <div class="clear"></div>
      <div class="double_line"></div>
      <h5>Venue</h5>
      <ul class="select-room">
        <li class="checked"><img src="images/content/img-atanaya.jpg" alt="">
          <div>
            <h2>Anjaya 1</h2>
            <span>30 - 50 person</span>
            <p>Our Best Four Meeting Room with Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
            <div class="radio">
              <input type="radio" name="radiog_lite" id="radio1" class="css-checkbox" checked/>
              <label for="radio1" class="css-label radGroup1"> SELECT </label>
            </div>
          </div>
        </li>
        <li><img src="images/content/img-atanaya.jpg" alt="">
          <div>
            <h2>Anjaya 1</h2>
            <span>30 - 50 person</span>
            <p>Our Best Four Meeting Room with Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
            <div class="radio">
              <input type="radio" name="radiog_lite" id="radio2" class="css-checkbox" />
              <label for="radio2" class="css-label radGroup1"> SELECT </label>
            </div>
          </div>
        </li>
        <li><img src="images/content/img-atanaya.jpg" alt="">
          <div>
            <h2>Anjaya 1</h2>
            <span>30 - 50 person</span>
            <p>Our Best Four Meeting Room with Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
            <div class="radio">
              <input type="radio" name="radiog_lite" id="radio3" class="css-checkbox" />
              <label for="radio3" class="css-label radGroup1"> SELECT </label>
            </div>
          </div>
        </li>
        <li><img src="images/content/img-atanaya.jpg" alt="">
          <div>
            <h2>Anjaya 1</h2>
            <span>30 - 50 person</span>
            <p>Our Best Four Meeting Room with Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
            <div class="radio">
              <input type="radio" name="radiog_lite" id="radio4" class="css-checkbox" />
              <label for="radio4" class="css-label radGroup1"> SELECT </label>
            </div>
          </div>
        </li>
        <li><img src="images/content/img-atanaya.jpg" alt="">
          <div>
            <h2>Anjaya 1</h2>
            <span>30 - 50 person</span>
            <p>Our Best Four Meeting Room with Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
            <div class="radio">
              <input type="radio" name="radiog_lite" id="radio5" class="css-checkbox" />
              <label for="radio5" class="css-label radGroup1"> SELECT </label>
            </div>
          </div>
        </li>
      </ul>
      <div class="double_line"></div>
      <h5>Date of Event</h5>
      <div class="left">
        <div>
          <label>Start Time of Meeting <span>*</span></label>
          <!--<select name="select" class="fm_select">
            <option>DD</option>
          </select>
          <select name="select" class="fm_select">
            <option>MM</option>
          </select>
          <select name="select" class="fm_select">
            <option>YYYY</option>
          </select>-->
          <input name="" type="text" class="datepicker" style="width:100px;">
        </div>
        <div>
          <label>Total Pax <span>*</span></label>
          <input name="" type="text">
        </div>
        <div>
          <label>Service requirement <span>*</span></label>
          <select name="select" class="long fm_select">
            <option value="Room Only">Room Only</option>
            <option value="Coffee Break Only">Coffee Break Only</option>
            <option value="Lunch Or Dinner Only">Lunch Or Dinner Only</option>
            <option value="Half Day Package">Half Day Package</option>
            <option value="Full Day Package">Full Day Package</option>
            <option value="Full Board Package">Full Board Package</option>
            <option value="Residential Package">Residential Package</option>
          </select>
        </div>
        
      </div>
      <div class="right">
        <div>
          <label>End Time of Meeting <span>*</span></label>
          <input name="" type="text" class="datepicker" style="width:100px;">
        </div>
        <div>
          <label>Set up request </label>
          <select name="select" class="long fm_select">
            <option value="Standing Party">Standing Party</option>
            <option value="Theatre">Theatre</option>
            <option value="Round Table">Round Table</option>
            <option value="Classroom">Classroom</option>
            <option value="U Shape">U Shape</option>
            <option value="Hollow / Block">Hollow / Block</option>
          </select>
        </div>
        <div>
          <label>Method of payment <span>*</span></label>
          <input type="radio" name="radiog_lite" id="radio10" class="css-checkbox" />
          <label for="radio10" class="css-label radGroup1"> CASH </label>
          <input type="radio" name="radiog_lite" id="radio11" class="css-checkbox" />
          <label for="radio11" class="css-label radGroup1"> Credit Card </label>
          <input type="radio" name="radiog_lite" id="radio12" class="css-checkbox" />
          <label for="radio12" class="css-label radGroup1"> Bank Transfer </label>
          <input type="radio" name="radiog_lite" id="radio13" class="css-checkbox" />
          <label for="radio13" class="css-label radGroup1"> Other </label>
        </div>
        <!--<div>
          <label>Promo Code</label>
          <input name="" type="text">
        </div>-->        
      </div>
      <div class="clear"></div>
      <label style="margin-top:15px; font-size:12px;"><span>*Required to fill</span></label>
      <div align="right">
          <input name="" type="submit" value="SUBMIT" class="btn red" style="margin-top:25px;">
        </div>
    </form>
  </div>
</section>
<!-- end of middle -->
<?php include('inc_footer.php');?>