<?php $page = "page4"; ?>
<?php include('inc_header.php');?>
<script src="js/jquery.carouFredSel-6.2.1-packed.js"></script> 
<!-- middle -->
<div id="banner-content"><img src="images/slider/banner-offer.jpg" alt=""></div>
<?php include("inc_booking.php"); ?>
<div id="breadcumb">
  <div class="wrapper"><a href="#">Home</a> / <a href="#">Special Offer</a></div>
</div>
<section id="main-content" class="with-flower">
  <div class="wrapper">
    <div class="title-detail">kapur sirih restaurant
      <nav class="socmed"> <a href="#"><img src="images/material/icon-socmed.png" width="32" height="32" alt=""></a> <a href="#"><img src="images/material/icon-socmed-02.png" width="32" height="32" alt=""></a> <a href="#"><img src="images/material/icon-socmed-03.png" width="32" height="32" alt=""></a> <a href="#"><img src="images/material/icon-socmed-04.png" width="32" height="32" alt=""></a> </nav>
    </div>
    <div id="dragable">
      <div class="box"><img src="images/content/img-kapur.jpg" alt=""></div>
      <div class="box"><img src="images/content/img-content-room.jpg" alt=""></div>
      <div class="box"><img src="images/content/img-kapur.jpg" alt=""></div>
      <div class="box"><img src="images/content/img-content-room.jpg" alt=""></div>
    </div>
    <p><strong>Opening Times 6:30 am to 12:00 am</strong></p>
    <p> Named after a cherished Balinese dinner tradition, the Kapur Sirih Restaurant is surrounded by elegant stone design and modern decor, embracing guests in a tropical peranakan atmosphere. </p>
    <p>Kapur Sirih serves authentic Indonesian cuisine called “Nyonya”, a blend of traditional Chinese and Indonesian cuisine, adapted over time and crafted with the freshest in local ingredients. Kapur Sirih‘s menu features an array of healthy culinary delights, including nasi, seafood and organic entrees, and both sweet and savory dishes. </p>
    <p> Alongside Kapur Sirih’s classic Nyonya, guests can enjoy their favorite Continental cuisine and selection of comfort foods from home.</p>
    <div class="double_line" style="margin-top:25px;"></div>
    <nav class="prevnext afterclear"><a href="#">PREVIOUS</a> <a href="#">NEXT</a></nav>
  </div>
</section>
<script src="http://cdnjs.cloudflare.com/ajax/libs/gsap/1.15.1/utils/Draggable.min.js"></script> 
<script src="http://cdnjs.cloudflare.com/ajax/libs/gsap/1.15.1/plugins/CSSPlugin.min.js"></script> 
<script src="js/ThrowPropsPlugin.min.js"></script> 
<script src="js/slider-dragable.js"></script> 
<script type="text/javascript">
	$(window).load(function(e) {
        $('#foo2').carouFredSel({
		  auto: false,
		  prev: '#prev2',
		  next: '#next2',
		  mousewheel: true,
		  swipe: {
			  onMouse: true,
			  onTouch: true
		  }
	  });
	  $("#dragable").slider_drag();
    });
</script> 
<!-- end of middle -->
<?php include('inc_footer.php');?>