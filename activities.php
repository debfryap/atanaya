<?php $page = "page2"; ?>
<?php include('inc_header.php');?>
<!-- middle -->
<div id="banner-content"><img src="images/slider/banner-offer.jpg" alt=""></div>
<?php include("inc_booking.php"); ?>
<div id="breadcumb">
  <div class="wrap_wide"><a href="#">Home</a> / <a href="#">Activities</a></div>
</div>
<section id="main-content">
  <div class="wrap_wide">
    <h2 class="title">Activities</h2>
    <p class="wording">The Atanaya Hotel features a variety of vacation packages pairing together the best rates on meeting and event facilities, banquet menus, amenities and accommodations. Explore the hotel’s packages for your next holiday to the tropical paradise of Bali.</p>
    <ul class="list-image afterclear home content" style="margin-top:40px;">
      <li>
        <div class="images"><a href="activities-detail.php"><img src="images/content/img-activities.jpg" alt="LIMITED TIME OFFER" ></a> </div>
        <p><a href="#">The Atanaya Hotel in Bali provides business guests with the best in meeting venue accommodations equipped with the latest in technology and service.</a></p>
      </li>
      <li>
        <div class="images"><a href="#"><img src="images/content/img-activities-05.jpg" alt="2 Nights Stay Special Deal" ></a> </div>
        <p><a href="#">The Atanaya Hotel in Bali provides business guests with the best in meeting venue accommodations equipped with the latest in technology and service.</a></p>
      </li>
      <li>
        <div class="images"><a href="#"><img src="images/content/img-activities-07.jpg" alt="Honeymoon Package" ></a> </div>
        <p><a href="#">The Atanaya Hotel in Bali provides business guests with the best in meeting venue accommodations equipped with the latest in technology and service.</a></p>
      </li>
      <li>
        <div class="images"><a href="#"><img src="images/content/img-activities-09.jpg" alt="Meeting Packages" ></a> </div>
        <p><a href="#">The Atanaya Hotel in Bali provides business guests with the best in meeting venue accommodations equipped with the latest in technology and service.</a></p>
      </li>
      <li>
        <div class="images"><a href="#"><img src="images/content/img-activities.jpg" alt="LIMITED TIME OFFER" ></a> </div>
        <p><a href="#">The Atanaya Hotel in Bali provides business guests with the best in meeting venue accommodations equipped with the latest in technology and service.</a></p>
      </li>
      <li>
        <div class="images"><a href="#"><img src="images/content/img-activities-05.jpg" alt="2 Nights Stay Special Deal" ></a> </div>
        <p><a href="#">The Atanaya Hotel in Bali provides business guests with the best in meeting venue accommodations equipped with the latest in technology and service.</a></p>
      </li>
      <li>
        <div class="images"><a href="#"><img src="images/content/img-activities-07.jpg" alt="Honeymoon Package" ></a> </div>
        <p><a href="#">The Atanaya Hotel in Bali provides business guests with the best in meeting venue accommodations equipped with the latest in technology and service.</a></p>
      </li>
      <li>
        <div class="images"><a href="#"><img src="images/content/img-activities-09.jpg" alt="Meeting Packages" ></a> </div>
        <p><a href="#">The Atanaya Hotel in Bali provides business guests with the best in meeting venue accommodations equipped with the latest in technology and service.</a></p>
      </li>
    </ul>
    <nav class="paging"><a href="#">&lt;</a><a href="#" class="active">1</a> <a href="#">2</a> <a href="#">3</a> <a href="#">4</a> <a href="#">5</a><a href="#">&gt;</a></nav>
  </div>
</section>
<!-- end of middle -->
<?php include('inc_footer.php');?>