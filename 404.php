<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Page Not Found</title>
<style>
@import url(http://fonts.googleapis.com/css?family=Roboto:400,300,700,900);
html, body {
	height: 100%;
	position: relative;
	margin: 0;
	font-family:'Roboto';
}
#page_404 {
	background-image: url(images/material/flower-content.png), url(images/material/flower-content-06.png);
	background-repeat: no-repeat, no-repeat;
	background-position: left 50px, right 90%;
	height: 100%;
	position: relative;
	text-align: center;
}
#page_404 .box{
	position: absolute;
	margin: auto;
	height: 230px;
	width: 239px;
	left: 0px;
	top: 0px;
	right: 0px;
	bottom: 0px;
}
#page_404 h1{
	font-size: 139px;
	color: #563a27;
	font-weight: 700;
	margin: 0px;
	line-height:130px;
}
#page_404 h2{
	font-size: 29px;
	text-transform: uppercase;
	color: #563a27;
	font-weight:300;
	margin: 0px;
}
#page_404 a{
	font-size: 14px;
	text-transform: uppercase;
	color: #fff;
	display: inline-block;
	padding: 10px;
	font-weight: 700;
	text-decoration: none;
	background-color: #c41319;
	width: 200px;
	text-align: center;
	-webkit-border-radius: 7px;
	-moz-border-radius: 7px;
	border-radius: 7px;
	margin-top: 15px;
}
</style>
</head>

<body>
<div id="page_404">
  <div class="box">
    <h1>404</h1>
    <h2>page not found</h2>
    <a href="index.php">BACK TO HOME</a></div>
</div>
</body>
</html>