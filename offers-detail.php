<?php $page = "page1"; ?>
<?php include('inc_header.php');?>
<script src="js/jquery.carouFredSel-6.2.1-packed.js"></script> 
<!-- middle -->
<div id="banner-content"><img src="images/slider/banner-offer.jpg" alt=""></div>
<?php include("inc_booking.php"); ?>
<div id="breadcumb">
  <div class="wrapper"><a href="#">Home</a> / <a href="#">Special Offers</a></div>
</div>
<section id="main-content" class="with-flower">
  <div class="wrapper">
    <div class="title-detail afterclear">honeymoon package
      <nav class="socmed"> <span>Share It</span> <a href="#"><img src="images/material/icon-socmed.png" width="32" height="32" alt=""></a> <a href="#"><img src="images/material/icon-socmed-02.png" width="32" height="32" alt=""></a> <a href="#"><img src="images/material/icon-socmed-03.png" width="32" height="32" alt=""></a> <a href="#"><img src="images/material/icon-socmed-04.png" width="32" height="32" alt=""></a> </nav>
    </div>
    <div id="dragable">
      <div class="box"><img src="images/content/img-kapur.jpg" alt="" width="1500" height="797"></div>
      <div class="box"><img src="images/content/img-content-room.jpg" alt="" width="1500" height="797"></div>
      <div class="box"><img src="images/content/img-kapur.jpg" alt="" width="1500" height="797"></div>
      <div class="box"><img src="images/content/img-content-room.jpg" alt="" width="1500" height="797"></div>
    </div>
    <p>Offering the holiday experience that a couple Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>
    <p>Stay Min 2 Nights and get a special benefits:</p>
    <ul>
      <li>Honeymoon set-up room</li>
      <li> Roundtrip Airport Transfer</li>
      <li> Romantic Dinner for 2</li>
      <li> Daily Buffet Breakfast for 2</li>
      <li> Free mineral water, coffee and tea making facilities in the room</li>
    </ul>
    <div class="call">
      <h5>Are you interested?</h5>
      <p>Please quote ‘HONEYMOON’ when booking.</p>
      <div class="right"><a href="#" class="btn line">CALL  (+62 361) 8468600</a> <span>OR</span> <a href="#" class="btn red">MAKE RESERVATION</a></div>
    </div>
    <div class="double_line"></div>
    <nav class="prevnext afterclear"><a href="#">PREVIOUS</a> <a href="#">NEXT</a></nav>
    <div class="list_carousel">
      <h5>OTHER Special Offers</h5>
      <ul class="list-image afterclear" id="foo2">
      <li>
        <div class="images"><img src="images/content/img-offer.jpg" alt="LIMITED TIME OFFER" >
          
        </div>
        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy .</p>
      </li>
      <li>
        <div class="images"><img src="images/content/img-offer-05.jpg" alt="2 Nights Stay Special Deal" >
          
        </div>
        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy .</p>
      </li>
      <li>
        <div class="images"><img src="images/content/img-offer-07.jpg" alt="Honeymoon Package" >
          
        </div>
        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy .</p>
      </li>
      <li>
        <div class="images"><img src="images/content/img-offer-09.jpg" alt="Meeting Packages" >
          
        </div>
        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy .</p>
      </li>
      <li>
        <div class="images"><img src="images/content/img-offer.jpg" alt="LIMITED TIME OFFER" >
          
        </div>
        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy .</p>
      </li>
      <li>
        <div class="images"><img src="images/content/img-offer-05.jpg" alt="2 Nights Stay Special Deal" >
          
        </div>
        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy .</p>
      </li>
      <li>
        <div class="images"><img src="images/content/img-offer-07.jpg" alt="Honeymoon Package" >
          
        </div>
        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy .</p>
      </li>
      <li>
        <div class="images"><img src="images/content/img-offer-09.jpg" alt="Meeting Packages" >
          
        </div>
        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy .</p>
      </li>
    </ul>
      <div class="clearfix"></div>
      <a id="prev2" class="prev" href="#">&lt;</a> <a id="next2" class="next" href="#">&gt;</a>
      <div id="pager2" class="pager"></div>
    </div>    
  </div>
</section>
<script src="http://cdnjs.cloudflare.com/ajax/libs/gsap/1.15.1/utils/Draggable.min.js"></script> 
<script src="http://cdnjs.cloudflare.com/ajax/libs/gsap/1.15.1/plugins/CSSPlugin.min.js"></script> 
<script src="js/ThrowPropsPlugin.min.js"></script> 
<script src="js/slider-dragable.js"></script> 
<script type="text/javascript">
	$(window).load(function(e) {
        $('#foo2').carouFredSel({
		  auto: false,
		  prev: '#prev2',
		  next: '#next2',
		  mousewheel: true,
		  swipe: {
			  onMouse: true,
			  onTouch: true
		  }
	  });
	  $("#dragable").slider_drag();
    });
</script> 
<!-- end of middle -->
<?php include('inc_footer.php');?>