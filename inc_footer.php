<!--Footer -->

<footer>
  <div class="wrapper">
    <div class="address">
      <p><strong>The Atanaya Hotel Bali</strong> Sunset Road No. 88A, Kuta - Bali</p>
      <p><a href="tel:+623618468600"><img src="images/material/img-contact.png" width="16" height="16" alt="" />(+62 361) 8468600</a> <a href="#"><img src="images/material/img-contact-02.png" width="16" height="16" alt="" />(+62 361) 8947041/2</a><a href="mailto:eservation@atanaya.com"><img src="images/material/img-contact-03.png" width="16" height="16" alt="" />reservation@atanaya.com</a></p>
      <nav><a href="sitemap.php">sitemap</a> <a href="privacy.php">privacy policy</a> <a href="media.php">media centre </a></nav>
    </div>
    <div class="center"><p>Managed by</p>
      <a href="http://unityhotels.com/" target="_blank"><img src="images/content/manageby.png" alt="" /></a>
      <p>&copy; Copyright 2015</p>
    </div>
    <div class="subscribe">
      <nav class="socmed"><a href="#"><img src="images/material/icon-socmed.png" width="32" height="32" alt="" /></a><a href="#"><img src="images/material/icon-socmed-02.png" width="32" height="32" alt="" /></a><a href="#"><img src="images/material/icon-socmed-03.png" width="32" height="32" alt="" /></a><a href="#"><img src="images/material/icon-socmed-04.png" width="32" height="32" alt="" /></a><a href="#"><img src="images/material/icon-socmed-05.png" width="32" height="32" alt="" /></a></nav>
      <form action="" method="get" id="subscribe">
      	<label>newsletter sign up</label>
        <input name="" type="text" placeholder="Your Email"/><br />
        <input name="" type="submit" value="Subscribe"/>
      </form>
    </div>
  </div>
</footer>
<!--end of Footer -->
<div style="display:none;">
<div id="thanks" class="pop-content">
    <h2 class="title">thanks for subscribing</h2>
    <div class="double_line"></div>
    <p>Anda telah berhasil berlangganan newletter Atanaya, 
dengan ini anda akan mendapatkan newsletter setiap minggu.
Dengan berlangganan newsletter Atanaya anda akan mengetahui 
jika ada promo/event terbaru dari kami</p>    
  </div>
</div>
</body></html>