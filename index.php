<?php $page = "page0"; ?>
<?php include('inc_header.php');?>
<script src="js/jquery.carouFredSel-6.2.1-packed.js"></script> 
<!-- middle -->
<div class="slider-wrapper theme-default">
  <div id="slider" class="nivoSlider"> <img src="images/slider/banner-01.jpg" alt="" title="#htmlcaption"/> <img src="images/slider/banner-02.jpg" alt="" title="#htmlcaption" /> <img src="images/slider/banner-01.jpg" alt="" title="#htmlcaption"/> <img src="images/slider/banner-02.jpg" alt="" title="#htmlcaption" /> </div>
  <div id="htmlcaption" class="nivo-html-caption"> <span class="small">welcome to</span> <span class="big">The Atanaya</span> </div>
</div>
<?php include("inc_booking.php"); ?>
<section id="welcome_text">
  <div class="wrapper">
    <h1 class="animated">best place to stay in bali</h1>
    <p class="animated">Everything about THE ATANAYA will make you feel as if you were staying in Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat.</p>
  </div>
</section>
<section id="offer">
  <div class="wrap_wide">
    <h2 class="title animated">What we offers</h2>
    <a href="#" class="link-arrow animated">see all promotions</a>
    <div class="list_carousel responsive">
      <ul class="list-image afterclear home list-animated" id="foo2">
        <li>
          <div class="images"><a href="#"><img src="images/content/img-offer.jpg" alt="LIMITED TIME OFFER" ></a> </div>
          <p><a href="#">The Atanaya Hotel in Bali provides business guests with the best in meeting venue accommodations equipped with the latest in technology and service.</a></p>
        </li>
        <li>
          <div class="images"><a href="#"><img src="images/content/img-offer-05.jpg" alt="2 Nights Stay Special Deal" ></a> </div>
          <p><a href="#">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy .</a></p>
        </li>
        <li>
          <div class="images"><a href="#"><img src="images/content/img-offer-07.jpg" alt="Honeymoon Package" ></a> </div>
          <p><a href="#">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy .</a></p>
        </li>
        <li>
          <div class="images"><a href="#"><img src="images/content/img-offer-09.jpg" alt="Meeting Packages" ></a> </div>
          <p><a href="#">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy .</a></p>
        </li>       
      </ul>
      <div class="clearfix"></div>
      <a id="prev2" class="prev" href="#">&lt;</a> <a id="next2" class="next" href="#">&gt;</a>
      <div id="pager2" class="pager"></div>
    </div>
  </div>
</section>
<section id="our-location">
  <div class="wrapper">
    <h2 class="title animated">our location</h2>
    <div class="maps" id="googleMaps"></div>
    <h4>Located in the heart of the famed beach resort town of Kuta</h4>
    <p class="animated">The Atanaya Kuta Bali Hotel is a tranquil retreat surrounded by temples, beaches, kind Balinese and a wealth of activities geared toward both adventure and relaxation.</p>
  </div>
</section>
<section id="thesay">
  <div class="wrapper"> <img src="images/material/tripadvisor.png" alt="">
    <h2 class="title animated">What they say</h2>
    <div class="column">
      <h4>“Comfort place to be”</h4>
      <p>Pool on the sixht floor is amazing, you can enjoy the view and get a light breeze on your body while laying in the sun bed</p>
      <strong>Hanskrs, Norway - TripAdvisor</strong></div>
    <div class="column">
      <h4>“Recommended place to stay”</h4>
      <p>The hotel is in a excellent location, not far from Kuta beach, Seminyak and Denpasar. Not only the hotel is in a strategic place, the room is nice and comfortable</p>
      <strong>NikoPrasetyo, Jakarta - TripAdvisor</strong></div>
    <div class="column">
      <h4>"Best place to stay"</h4>
      <p>Great place to stay, rooms are very clean, very friendly staff. Would definitely recommend Atanaya Hotel for their best service.</p>
      <strong>Hemijack, Spain - TripAdvisor</strong></div>
  </div>
</section>
<!-- end of middle -->
<div style="display:none;">
  <div id="startup" class="pop-content">
    <h4>honeymoon package</h4>
    <img src="images/content/img-startup.jpg" alt="">
    <p>Offering the holiday experience that a couple Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat
      Stay Min 2 Nights and get a special benefits:</p>
    <ul>
      <li>Honeymoon set-up room</li>
      <li> Roundtrip Airport Transfer</li>
      <li> Romantic Dinner for 2</li>
      <li> Daily Buffet Breakfast for 2</li>
      <li> Free mineral water, coffee and tea making<br>
        facilities</li>
    </ul>
    <div class="call afterclear">
      <h5>Are you interested?</h5>
      <p>Please quote ‘HONEYMOON’ when booking.</p>
      <div class="right"><a href="#" class="btn red">BOOK NOW</a></div>
    </div>
    <p class="note">Note : Offering the holiday experience that a couple Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>
  </div>
</div>
<script type="text/javascript">
    $(window).load(function() {		
		function anim(){
			TweenMax.to('.nivo-caption .small',1,{ opacity : 1,rotationX:0});	
			TweenMax.to('.nivo-caption .big',0.5,{ opacity : 1,marginTop : 0, delay: 1});	
		}	
		
        $('#slider').nivoSlider({
			controlNav: true,
			pauseTime: 5000,
			beforeChange: function(){},
    		afterChange: function(){
				anim();		
			},
			afterLoad: function(span1){
				anim();	
			}
		});
		$.fancybox.open([{
			href : '#startup'	
		}])
		$('#foo2').carouFredSel({
			width: '100%',
		  auto: false,
		  prev: '#prev2',
		  next: '#next2',
		  mousewheel: true,
		  swipe: {
			  onMouse: true,
			  onTouch: true
		  },		  
	  });
    });
</script> 
<script type = "text/javascript" src = "https://maps.googleapis.com/maps/api/js?v=3&amp;sensor=false" >  </script> <script> function initialize() {
	var poin = new google.maps.LatLng(-8.703571, 115.180356);
	var mapStyle = [{
			"featureType" : "road.highway.controlled_access",
			"stylers" : [{
					"visibility" : "on"
				}, {
					"color" : "#c2c2c2"
				}
			]
		}, {
			"featureType" : "water",
			"stylers" : [{
					"visibility" : "on"
				}, {
					"color" : "#47c0ef"
				}
			]
		}, {
			"featureType" : "road.arterial",
			"elementType" : "labels.text.fill",
			"stylers" : [{
					"visibility" : "on"
				}, {
					"color" : "#a8a8a8"
				}
			]
		}, {
			"featureType" : "road.highway",
			"elementType" : "labels.text.fill",
			"stylers" : [{
					"color" : "#9b9b9b"
				}
			]
		}, {
			"featureType" : "landscape.man_made",
			"stylers" : [{
					"hue" : "#0091ff"
				}, {
					"color" : "#f2f2f2"
				}
			]
		}
	];
	var styledMap = new google.maps.StyledMapType(mapStyle, {
			name : "Styled Map"
		});
	var mapProp = {
		center : poin,
		zoom : 16,
		panControl : false,
		zoomControl : false,
		mapTypeControl : false,
		mapTypeControlOptions : {
			mapTypeIds : [google.maps.MapTypeId.ROADMAP, 'map_style']
		}
	};
	var map = new google.maps.Map(document.getElementById("googleMaps"), mapProp);
	var marker = new google.maps.Marker({
			position : poin,
			icon : "images/material/pointer2.png"
		});
	marker.setMap(map);
	map.mapTypes.set('map_style', styledMap);
	map.setMapTypeId('map_style');
	google.maps.event.addListener(marker, 'click', function() {
		window.open("https://www.google.co.id/maps/place/Atanaya+Hotel+-+Kuta,+Bali/@-8.703549,115.180501,17z/data=!3m1!4b1!4m2!3m1!1s0x2dd246ce303f6449:0x5ff7f4647568d7e2", '_blank');		
  });
}
google.maps.event.addDomListener(window, 'load', initialize);
</script>
<?php include('inc_footer.php');?>