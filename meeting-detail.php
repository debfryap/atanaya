<?php $page = "page5"; ?>
<?php include('inc_header.php');?>
<script src="js/jquery.carouFredSel-6.2.1-packed.js"></script> 
<!-- middle -->
<div id="banner-content"><img src="images/slider/banner-offer.jpg" alt=""></div>
<?php include("inc_booking.php"); ?>
<div id="breadcumb">
  <div class="wrapper"><a href="#">Home</a> / <a href="#">Meeting</a></div>
</div>
<section id="main-content" class="with-flower">
  <div class="wrapper">
    <div class="title-detail">Anjaya 1, 2, 3
      <nav class="socmed"> <a href="#"><img src="images/material/icon-socmed.png" width="32" height="32" alt=""></a> <a href="#"><img src="images/material/icon-socmed-02.png" width="32" height="32" alt=""></a> <a href="#"><img src="images/material/icon-socmed-03.png" width="32" height="32" alt=""></a> <a href="#"><img src="images/material/icon-socmed-04.png" width="32" height="32" alt=""></a> </nav>
    </div>
    <div id="dragable">
      <div class="box"><img src="images/content/img-kapur.jpg" alt=""></div>
      <div class="box"><img src="images/content/img-content-room.jpg" alt=""></div>
      <div class="box"><img src="images/content/img-kapur.jpg" alt=""></div>
      <div class="box"><img src="images/content/img-content-room.jpg" alt=""></div>
    </div>
    <p> The Atanaya Hotel’s meeting rooms are ideal for private business meetings or special events, for up to 100 people. The Anjaya meeting rooms can be customized to fit your event’s specifications, and provide a contemporary surrounding for your guests influenced by the physical and spiritual paradise of Bali. </p>
    <p>Each meeting venue is fully air-conditioned and features complimentary Internet and Wi-Fi connections. Enhanced audio visual technology features are also available to support your needs.</p>
    <div id="data-table">
      <h5>Meeting Room Configurations</h5>
      <a href="#" class="icon-pdf">Download Meeting Room Configurations</a>
<table width="100%" border="0" class="data_table" style="min-width:960px">
        <thead>
          <tr>
            <td>Room</td>
            <td>Dimension</td>
            <td>Theatre</td>
            <td>Class Room</td>
            <td>Black Table</td>
            <td>U-shape</td>
            <td>Round Table</td>
            <td>Standing Party</td>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>Anjaya 1 & 2</td>
            <td>17 x 5 M</td>
            <td>84 person</td>
            <td>60 person</td>
            <td>54 person</td>
            <td>54 person</td>
            <td>40 person</td>
            <td>100 person</td>
          </tr>
          <tr>
            <td>Anjaya 1 & 2</td>
            <td>17 x 5 M</td>
            <td>84 person</td>
            <td>60 person</td>
            <td>54 person</td>
            <td>54 person</td>
            <td>40 person</td>
            <td>100 person</td>
          </tr>
          <tr>
            <td>Anjaya 1 & 2</td>
            <td>17 x 5 M</td>
            <td>84 person</td>
            <td>60 person</td>
            <td>54 person</td>
            <td>54 person</td>
            <td>40 person</td>
            <td>100 person</td>
          </tr>
          <tr>
            <td>Anjaya 1 & 2</td>
            <td>17 x 5 M</td>
            <td>84 person</td>
            <td>60 person</td>
            <td>54 person</td>
            <td>54 person</td>
            <td>40 person</td>
            <td>100 person</td>
          </tr>
        </tbody>
      </table>
    </div>
    <div class="call">
      <h5>Are you interested?</h5>
      <div class="right"><a href="#" class="btn line" style="margin-right:10px;">Meeting Packages</a> <a href="#" class="btn line">CALL  (+62 361) 8468600</a> <span>OR</span> <a href="meeting-detail-02.php" class="btn red">MEETING ENQUIRY</a></div>
    </div>
    <div class="double_line" style="margin-top:25px;"></div>
    <nav class="prevnext afterclear"><a href="#">PREVIOUS ROOM</a> <a href="#">NEXT ROOM</a></nav>
  </div>
</section>
<script src="http://cdnjs.cloudflare.com/ajax/libs/gsap/1.15.1/utils/Draggable.min.js"></script> 
<script src="http://cdnjs.cloudflare.com/ajax/libs/gsap/1.15.1/plugins/CSSPlugin.min.js"></script> 
<script src="js/ThrowPropsPlugin.min.js"></script> 
<script src="js/slider-dragable.js"></script> 
<script type="text/javascript">
	$(window).load(function(e) {
        $('#foo2').carouFredSel({
		  auto: false,
		  prev: '#prev2',
		  next: '#next2',
		  mousewheel: true,
		  swipe: {
			  onMouse: true,
			  onTouch: true
		  }
	  });
	  $("#dragable").slider_drag();
    });
</script> 
<!-- end of middle -->
<?php include('inc_footer.php');?>