<?php $page = "page4"; ?>
<?php include('inc_header.php');?>
<!-- middle -->
<div id="banner-content"><img src="images/slider/banner-offer.jpg" alt=""></div>
<?php include("inc_booking.php"); ?>
<div id="breadcumb">
  <div class="wrapper"><a href="#">Home</a> / <a href="#">Restaurant</a></div>
</div>
<section id="main-content" class="with-flower">
  <div class="wrapper">
    <h2 class="title">Restaurant</h2>
    <p class="wording">kita memiliki 4 kamar berbeda dengan fasilitas kamar Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore  volutpat ipsum magna aliquam erat volutpat ipsum dolor sit amet, consectetuer .</p>
    <div class="double_line"></div>
    <ul class="list-room">
      <li><div class="images"><a href="restaurant-detail.php"><img src="images/content/img-room.jpg" alt=""></a></div>
        <div class="box">
          <h4><a href="#">Kapur Sirih Restaurant</a></h4>
<!--          <p>surrounded by elegant stone design and modern decor</p>-->
        </div>
      </li>
      <li><div class="images"><a href="restaurant-detail.php"><img src="images/content/img-room-06.jpg" alt=""></a></div>
        <div class="box">
          <h4><a href="#">Brewu Coffee & Pastry</a></h4>
<!--          <p>guests can relax in the café’s open-air lounge</p>-->
        </div>
      </li>
      <li><div class="images"><a href="restaurant-detail.php"><img src="images/content/img-room-08.jpg" alt=""></a></div>
        <div class="box">
          <h4><a href="#">Sky 8 & Pool</a></h4>
<!--          <p>offers panoramic views of Kuta</p>-->
        </div>
      </li>
    </ul>
  </div>
</section>

<!-- end of middle -->
<?php include('inc_footer.php');?>