<?php $page = "page5"; ?>
<?php include('inc_header.php');?>
<!-- middle -->
<div id="banner-content"><img src="images/slider/banner-offer.jpg" alt=""></div>
<?php include("inc_booking.php"); ?>
<div id="breadcumb">
  <div class="wrapper"><a href="#">Home</a> / <a href="#">Meeting</a></div>
</div>
<section id="main-content" class="with-flower">
  <div class="wrapper">
    <h2 class="title">Meeting</h2>
    <p class="wording">kita memiliki 4 kamar berbeda dengan fasilitas kamar Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore  volutpat ipsum magna aliquam erat volutpat ipsum dolor sit amet, consectetuer .</p>
    <div class="double_line"></div>
    <ul class="list-room">
      <li><div class="images"><a href="meeting-detail.php"><img src="images/content/img-meetingroom.jpg" alt=""></a></div>
        <div class="box">
          <h4><a href="#">Anjaya 1, 2, 3</a></h4>
<!--          <p>ideal for private business meetings or special events</p>-->
        </div>
      </li>
      <li><div class="images"><a href="meeting-detail.php"><img src="images/content/img-meetingroom-06.jpg" alt=""></a></div>
        <div class="box">
          <h4><a href="#">Atiharsa</a></h4>
<!--          <p>affords guests panoramic views of Bali</p>-->
        </div>
      </li>
      <li><div class="images"><a href="meeting-detail.php"><img src="images/content/img-room-08.jpg" alt=""></a></div>
        <div class="box">
          <h4><a href="#">Breakout room</a></h4>
<!--          <p>total functional and privacy for your meetings</p>-->
        </div>
      </li>
    </ul>
  </div>
</section>

<!-- end of middle -->
<?php include('inc_footer.php');?>