<?php $page = "page0"; ?>
<?php include('inc_header.php');?>
<!-- middle -->
<div id="banner-content"><img src="images/slider/banner-offer.jpg" alt=""></div>
<?php include("inc_booking.php"); ?>
<div id="breadcumb">
  <div class="wrapper"><a href="#">Home</a> / <a href="#">Meeting</a></div>
</div>
<section id="main-content" class="with-flower">
  <div class="wrapper">
    <nav class="media afterclear"><a href="media.php" class="active">photos & videos</a> <a href="media-2.php">press release</a> <a href="media-3.php">publications</a></nav>
    <div class="title-detail">Photo & video
      <nav class="socmed"><a href="#"><img src="images/material/icon-socmed.png" width="32" height="32" alt=""></a> <a href="#"><img src="images/material/icon-socmed-02.png" width="32" height="32" alt=""></a> <a href="#"><img src="images/material/icon-socmed-03.png" width="32" height="32" alt=""></a> <a href="#"><img src="images/material/icon-socmed-04.png" width="32" height="32" alt=""></a> </nav>
    </div>
    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in .</p>
    <ul class="list-image afterclear media" style="margin-top:40px;">
      <li>
        <div class="images"><a href="images/content/img-popup.jpg" class="fancybox-effects-c" data-title="This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. "><img src="images/content/img-offer.jpg" alt="LIMITED TIME OFFER"></a> </div>
      </li>
      <li class="video" data-url="https://www.youtube.com/watch?v=TzvWja2Jxe8">
        <div class="images"><a href="https://www.youtube.com/watch?v=TzvWja2Jxe8" class="fancybox-media"><img src="images/content/img-offer-05.jpg" alt="2 Nights Stay Special Deal" ></a> </div>
      </li>
      <li>
        <div class="images"><img src="images/content/img-offer-07.jpg" alt="Honeymoon Package" > </div>
      </li>
      <li>
        <div class="images"><img src="images/content/img-offer-09.jpg" alt="Meeting Packages" > </div>
      </li>
      <li>
        <div class="images"><img src="images/content/img-offer.jpg" alt="LIMITED TIME OFFER" > </div>
      </li>
      <li>
        <div class="images"><img src="images/content/img-offer-05.jpg" alt="2 Nights Stay Special Deal" > </div>
      </li>
      <li>
        <div class="images"><img src="images/content/img-offer-07.jpg" alt="Honeymoon Package" > </div>
      </li>
      <li>
        <div class="images"><img src="images/content/img-offer-09.jpg" alt="Meeting Packages" > </div>
      </li>
      <li>
        <div class="images"><img src="images/content/img-offer-09.jpg" alt="Meeting Packages" > </div>
      </li>
    </ul>
    <nav class="paging"><a href="#">&lt;</a><a href="#" class="active">1</a> <a href="#">2</a> <a href="#">3</a> <a href="#">4</a> <a href="#">5</a><a href="#">&gt;</a></nav>
  </div>
</section>
<script type="text/javascript" src="js/jquery.fancybox-media.js"></script>
<script type="text/javascript">
	$(window).load(function(e) {
		$(".fancybox-effects-c").click(function(e) {
			$this = $(this)
            $.fancybox.open([{
				href : $(this).attr("href"),
				afterShow : function(){
					setTimeout(function(){
						$(".fancybox-inner").css('height','auto');
					},2);					
					$(".fancybox-inner").append("<div class='text'><h4>"+$this.children("img").attr("alt")+"</h4><p>"+$this.attr("data-title")+"</p></div>");					
				}
			}]);
			return false;
        });
		$('.fancybox-media').fancybox({
		openEffect  : 'none',
		closeEffect : 'none',
		helpers : {
			media : {}
		}
	});
        
    });
</script> 
<!-- end of middle -->
<?php include('inc_footer.php');?>