<?php $page = "page6"; ?>
<?php include('inc_header.php');?> 
<!-- middle -->
<div id="banner-content"><img src="images/slider/banner-offer.jpg" alt=""></div>
<?php include("inc_booking.php"); ?>
<div id="breadcumb">
  <div class="wrapper"><a href="#">Home</a> / <a href="#">Meeting</a></div>
</div>
<section id="main-content" class="with-flower">
  <div class="wrapper">
    <div id="mapContact" style="height:250px"></div>
    <h2 class="title">How can we assist you ? </h2>    
    <form action="meeting-detail-03.php" method="get" class="form_style afterclear">
    <div class="double_line"></div>      
      <div class="left">
        <div>
          <label>Name<span>*</span></label>
          <input name="" type="text">
        </div>
        <div>
          <label>Telephone <span>*</span></label>
          <input name="" type="text">
        </div>
        <div>
          <label>Email <span>*</span></label>
          <input name="" type="text">
        </div>
      </div>
      <div class="right">
        <div>
          <label>Messages <span>*</span></label>
          <textarea name=""></textarea>
        </div>
        <div>
         <input name="" type="checkbox" value="" id="chck1"> <label for="chck1" class="chck">Yes, I want to receive the newsletter</label>    <input name="" type="submit" class="btn red right" value="SUBMIT">
        </div>
        
      </div>
      <div class="clear"></div>      
    </form>  
  </div>
</section>
<!-- end of middle -->
<script type = "text/javascript" src = "https://maps.googleapis.com/maps/api/js?v=3&amp;sensor=false" >  </script>
<script> function initialize() {
	var poin = new google.maps.LatLng(-8.703571, 115.180356);
	var mapStyle = [{
			"featureType" : "road.highway.controlled_access",
			"stylers" : [{
					"visibility" : "on"
				}, {
					"color" : "#c2c2c2"
				}
			]
		}, {
			"featureType" : "water",
			"stylers" : [{
					"visibility" : "on"
				}, {
					"color" : "#47c0ef"
				}
			]
		}, {
			"featureType" : "road.arterial",
			"elementType" : "labels.text.fill",
			"stylers" : [{
					"visibility" : "on"
				}, {
					"color" : "#a8a8a8"
				}
			]
		}, {
			"featureType" : "road.highway",
			"elementType" : "labels.text.fill",
			"stylers" : [{
					"color" : "#9b9b9b"
				}
			]
		}, {
			"featureType" : "landscape.man_made",
			"stylers" : [{
					"hue" : "#0091ff"
				}, {
					"color" : "#f2f2f2"
				}
			]
		}
	];
	var styledMap = new google.maps.StyledMapType(mapStyle, {
			name : "Styled Map"
		});
	var mapProp = {
		center : poin,
		zoom : 16,
		panControl : false,
		zoomControl : false,
		mapTypeControl : false,
		mapTypeControlOptions : {
			mapTypeIds : [google.maps.MapTypeId.ROADMAP, 'map_style']
		}
	};
	var map = new google.maps.Map(document.getElementById("mapContact"), mapProp);
	var marker = new google.maps.Marker({
			position : poin,
			icon : "images/material/pointer2.png"
		});
	marker.setMap(map);
	map.mapTypes.set('map_style', styledMap);
	map.setMapTypeId('map_style');
	google.maps.event.addListener(marker, 'click', function() {
		window.open("https://www.google.co.id/maps/place/Atanaya+Hotel+-+Kuta,+Bali/@-8.703549,115.180501,17z/data=!3m1!4b1!4m2!3m1!1s0x2dd246ce303f6449:0x5ff7f4647568d7e2", '_blank');		
  });
}
google.maps.event.addDomListener(window, 'load', initialize);
</script>
<?php include('inc_footer.php');?>