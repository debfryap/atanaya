<?php $page = "page0"; ?>
<?php include('inc_header.php');?> 
<!-- middle -->
<div id="banner-content"><img src="images/slider/banner-offer.jpg" alt=""></div>
<?php include("inc_booking.php"); ?>
<div id="breadcumb">
  <div class="wrapper"><a href="#">Home</a> / <a href="#">Meeting</a></div>
</div>
<section id="main-content" class="with-flower">
  <div class="wrapper">
  <h2 class="title">sitemap</h2>
   <div id="sitemap">
   	<div>
    	<a href="#" class="main">PROMOTION &amp; EVENT</a>
        <a href="#" class="sub">PROMOTION</a>
        <a href="#" class="child">Limited Time Offer</a>
        <a href="#" class="child">Honeymoon Package</a>
        <a href="#" class="child">Meeting Packages</a>
        <a href="#" class="child">2 Night Stay Special</a>
        <a href="#" class="child">Advance Puchase</a>
        <a href="#" class="sub">EVENT </a>
    </div>
    <div>
    	<a href="#" class="main">ROOMS</a>
        <a href="#">Deluxe Room</a>
        <a href="#">Excecutive Room</a>
        <a href="#">Junior Suite</a>
        <a href="#">Excecutive Suite </a>
    </div>
    <div>
    	<a href="#" class="main">RESTAURANT &amp; POOL</a>
          <a href="#">Kapur Sirih</a>
          <a href="#">Brewu coffee &amp; Pastry</a>
          <a href="#">Sky 8 pool</a>
    </div>
    <div>
    	<a href="#" class="main">MEETING</a>
          <a href="#">Anjaya 1,2,3</a>
          <a href="#">Atiharsa</a>
        <a href="#">Breakout Room</a>
        <a href="#" class="main">CONTACT US</a>
    </div>
   </div>   
  </div>
</section>
<!-- end of middle -->
<?php include('inc_footer.php');?>