<?php $page = "page0"; ?>
<?php include('inc_header.php');?>
<script src="js/jquery.carouFredSel-6.2.1-packed.js"></script> 
<!-- middle -->
<div id="banner-content"><img src="images/slider/banner-offer.jpg" alt=""></div>
<?php include("inc_booking.php"); ?>
<div id="breadcumb">
  <div class="wrapper"><a href="#">Home</a> / <a href="#">Meeting</a></div>
</div>
<section id="main-content" class="with-flower">
  <div class="wrapper">
  <nav class="media afterclear"><a href="media.php">photos & videos</a> <a href="media-2.php" class="active">press release</a> <a href="media-3.php">publications</a></nav>
    <div class="title-detail">press release
      <nav class="socmed"><a href="#"><img src="images/material/icon-socmed.png" width="32" height="32" alt=""></a> <a href="#"><img src="images/material/icon-socmed-02.png" width="32" height="32" alt=""></a> <a href="#"><img src="images/material/icon-socmed-03.png" width="32" height="32" alt=""></a> <a href="#"><img src="images/material/icon-socmed-04.png" width="32" height="32" alt=""></a> </nav>
    </div>
    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in .</p>
    <ul class="list-download">
      <li>
        <div class="wrap">
          <div class="time">8 agustus 2014, jakarta</div>
          <h4>Lorem ipsum dolor sit amet, consectetur adipiscing elit</h4>
          <a href="#">Download PDF</a> </div>
      </li>
      <li>
        <div class="wrap">
          <div class="time">8 agustus 2014, jakarta</div>
          <h4>Lorem ipsum dolor sit amet, consectetur adipiscing elit</h4>
          <a href="#">Download PDF</a> </div>
      </li>
      <li>
        <div class="wrap">
          <div class="time">8 agustus 2014, jakarta</div>
          <h4>Lorem ipsum dolor sit amet, consectetur adipiscing elit</h4>
          <a href="#">Download PDF</a> </div>
      </li>
      <li>
        <div class="wrap">
          <div class="time">8 agustus 2014, jakarta</div>
          <h4>Lorem ipsum dolor sit amet, consectetur adipiscing elit</h4>
          <a href="#">Download PDF</a> </div>
      </li>
      <li>
        <div class="wrap">
          <div class="time">8 agustus 2014, jakarta</div>
          <h4>Lorem ipsum dolor sit amet, consectetur adipiscing elit</h4>
          <a href="#">Download PDF</a> </div>
      </li>
      <li>
        <div class="wrap">
          <div class="time">8 agustus 2014, jakarta</div>
          <h4>Lorem ipsum dolor sit amet, consectetur adipiscing elit</h4>
          <a href="#">Download PDF</a> </div>
      </li>
      <li>
        <div class="wrap">
          <div class="time">8 agustus 2014, jakarta</div>
          <h4>Lorem ipsum dolor sit amet, consectetur adipiscing elit</h4>
          <a href="#">Download PDF</a> </div>
      </li>
      <li>
        <div class="wrap">
          <div class="time">8 agustus 2014, jakarta</div>
          <h4>Lorem ipsum dolor sit amet, consectetur adipiscing elit</h4>
          <a href="#">Download PDF</a> </div>
      </li>
      <li>
        <div class="wrap">
          <div class="time">8 agustus 2014, jakarta</div>
          <h4>Lorem ipsum dolor sit amet, consectetur adipiscing elit</h4>
          <a href="#">Download PDF</a> </div>
      </li>
    </ul>
    <nav class="paging"><a href="#">&lt;</a><a href="#" class="active">1</a> <a href="#">2</a> <a href="#">3</a> <a href="#">4</a> <a href="#">5</a><a href="#">&gt;</a></nav>
  </div>
</section>
<script type="text/javascript">
	$(window).load(function(e) {
        $('#foo2').carouFredSel({
		  auto: false,
		  prev: '#prev2',
		  next: '#next2',
		  mousewheel: true,
		  swipe: {
			  onMouse: true,
			  onTouch: true
		  }
	  });
	  $('#slider').nivoSlider({
			controlNav: true,
			directionNav: false,
			pauseTime: 5000,			
		});
    });
</script> 
<!-- end of middle -->
<?php include('inc_footer.php');?>