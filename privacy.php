<?php $page = "page5"; ?>
<?php include('inc_header.php');?> 
<!-- middle -->
<div id="banner-content"><img src="images/slider/banner-offer.jpg" alt=""></div>
<?php include("inc_booking.php"); ?>
<div id="breadcumb">
  <div class="wrapper"><a href="#">Home</a> / <a href="#">Privacy Policy</a></div>
</div>
<section id="main-content" class="with-flower">
  <div class="wrapper">
  <h2 class="title">Privacy</h2>
<!--   <p align="center">Pursuant to Legislative Decree No. 196 of June 30, 2003 ("Code relating to the protection of personal data"), we would like to inform all users that the data they provide when making inquiries or reservations may be used for processing, advertising, and/or other activities of us or our partners in the tourism and hotel sector. The data are processed in written form and/or on magnetic media, electronic media and in compliance with all security measures guaranteeing their security and confidentiality. Hotel La Perla is the responsible for data processing. Affected parties can check, correct, supplement, or delete their data at any time by writing to Hotel La Perla, Col Alt 105 str., I-39033 Corvara (BZ) Alta Badia or to the following e-mail address info@hotel-laperla.it.
</p>-->
<p>This notice describes the general privacy policy of the Atanaya Hotel Bali*. By visiting www.atanaya.com, you are accepting the practices described in this notice.</p>

<p>Q.1 What Information About You Do We Gather?</p>

<p>Q.2 What About Cookies?</p>

<p>Q.3 Does Atanaya Hotel Bali Share the Information It Receives?</p>

<p>Q.4 Does Atanaya Hotel Bali Transfer Information Overseas?</p>

<p>Q.5 How Secure Is Information About Me?</p>

<p>Q.6 What Can I Do If I Have any Questions or Concerns or Want to Access My Personal Data? </p>

<p><strong>1. WHAT INFORMATION ABOUT YOU DO WE GATHER?</strong></p>

<p>Information you give us helps us to provide you with our services, enter into or fulfill a contract with you and/or reply to or take actions in response to your enquiries or requests. The types of information we gather are:</p>

<p>Information You Give Us: We receive and store information you enter on our website or give us in any other way, including, for example, when you stay as a guest at hotels managed by us. The provision of any information by you is voluntary except where otherwise stated. However, where you choose not to provide certain information, we may not be able to reply or respond to your enquiry or request and you may not be able to take advantage of some or all of our services. We use the information that you provide for such purposes as entering into or fulfilling a contract with you, reserving a room, processing on-line purchase transactions (including purchase of Atanaya gift cards), replying to or taking actions in response to your enquiries or requests, and customizing our services to your preferences. We also use such information for statistical and analysis purposes, to communicate news and promotions to you relating to Atanaya Hotel Bali-related products and services and other products and services we think may be of interest to you, and otherwise communicating with you by email, mail, fax, phone or other means. We may also use such information as may be required to comply with applicable local laws.<br />

  Personal Data You Provided on Behalf of Another Person: If you are entering data on behalf of another person, you warrant to us that you are authorised by that person to enter their personal data onto our system, and that that information is accurate and correct. If any non-compliance by you with respect to this provision results in any loss or damage being incurred by us, you may be required to compensate us in respect of such loss. Automatic Information: We receive and may evaluate certain types of information whenever you interact with us. For example, like many websites, we obtain certain types of information when your Web browser accesses www.unityhotel.com including your IP address, browser type and operating system. This information helps us to communicate with our customers and better understand them.<br />

  E-mail Communications: If you provide us with your email address, we may email you promotional and marketing materials. We provide you with the option not to receive such promotional and marketing materials in each email sent to you. If you stay as a guest at hotels or residences properties managed by us, we may also use your email address to send you a Guest Satisfaction Survey after your stay. If you do not wish to receive a survey, you may notify us by contacting our Data Privacy Officer at data-protection@unity.com. You may also receive an auto-generated e-mail confirmation when making a reservation on the Internet for stays at hotels or residences properties managed by us, when consummating any on-line purchase transactions with us, and occasionally from our reservation sales agents for inquiries related to a booking you have made for any of our services and products.<br />

  Information from Other Sources: For reasons such as improving personalisation of our services (for example, providing recommendations or special offers that we think will interest you), we might receive information about you from other sources and add it to your existing information. We may share your existing information with third parties where this is necessary to provide you with our services, enter into or fulfill a contract with you and/or reply to or take actions in response to your enquiries or requests.<br />

  Age Limitations: Atanaya does not knowingly collect personally identifiable information from our websites from any person we actually know is a person under the age of 18. Atanaya may collect personally identifiable information from persons under the age of 18 as part of the guest registration process, but always with the consent of such person's parent or guardian.</p>

<p><strong>2. WHAT ABOUT COOKIES?</strong></p>

<p>Some websites store, and so does www.unityhotel.com, information in a small text file, called a &quot;cookie,&quot; on your hard disk.</p>

<p>Cookies contain information about you and your preferences. For example, if you inquire about rates and availability, the site might create a cookie that contains the details you entered. Or it might only contain a record of which pages within the site you visited, to help the site customise the view for you the next time you visit.</p>

<p>Only the information that you provide, or the choices you make while visiting a website, can be stored in a cookie. For example, the site cannot determine your e-mail name unless you choose to type it. Allowing a website to create a cookie does not give that or any other site access to the rest of your computer, and only the site that created the cookie can read it.</p>

<p>Most browsers are initially set to accept cookies, but you can set your browser to alert you every time a cookie is offered, letting you decide whether or not to accept it. You can prevent the use of cookies by modifying your browser, although you may not be able to use some of our services if you prevent the use of cookies.</p>

<p>3. DOES Atanaya Hotel Bali SHARE THE INFORMATION IT RECEIVES?</p>

<p>Information you provide to us will be used for such purposes as entering into or fulfilling a contract with you, reserving a room, processing on-line purchase transactions (including purchase of Atanaya gift cards), replying to or taking actions in response to your enquiries or requests, and customizing our services to your preferences. We also use such information for statistical and analysis purposes, to communicate news and promotions to you relating to Unityl Hotel Group-related products and services and other products and services we think may be of interest to you, and otherwise communicating with you by email, mail, fax, phone or other means. We may also use such information as may be required to comply with applicable local laws. We may therefore share information amongst ourselves, with hotels and residences properties managed by us, with third parties that collaborate with us on joint promotions and with third party service providers that assist in managing your data and communicating news and promotions to you. In addition, we may share your information with other third parties where it is reasonably necessary or required in order to enter into or fulfill a contract with you and/or to reply to or take actions in response to your enquiries or requests, and as may be required to comply with applicable local laws.</p>

<p><strong>4. DOES Atanaya Hotel Bali TRANSFER INFORMATION OVERSEAS?</strong></p>

<p>We may transfer your information outside of the country in which it was collected (including to countries where we have hotels under development or operation) for various reasons, including the purpose of entering into or fulfilling a contract with you, reserving a room, processing on-line purchase transactions (including purchase of Atanaya gift cards), replying to or taking actions in response to your enquiries or requests, for processing by us or on behalf of the hotels and residences properties managed by us, for enhancing personalization of services provided to you, for communicating news and promotions to you relating to Atanaya Hotel Bali-related products and services and other products and services we think may be of interest to you, and for statistical and analysis purposes. Such transfer may be to a country which may not provide the same level of privacy protection as that provided by the country in which the information was collected but we will take reasonable steps (including entering into data transfer agreements based on the European Commission model clauses, where required) to ensure that while your data is within our control, we will handle your data using reasonable security and confidentiality procedures to keep your data secure and confidential.</p>

<p><strong>5. HOW SECURE IS INFORMATION ABOUT ME?</strong></p>

<p>We work to protect the security of your information during transmission by using Secure Sockets Layer (SSL) technology, which encrypts information you input and which is certified by the Secure Server Certification Authority.<br />

  We reveal only the last four digits of your credit card numbers when confirming a reservation or processing on-line purchase transactions. Of course, we transmit the entire credit card number to the appropriate credit card company for verification.<br />

  It is important for you to protect against unauthorized access to your password and to your computer. Be sure to sign off when you have finished using a shared computer.</p>

<p><strong>6. WHAT CAN I DO IF I HAVE ANY QUESTIONS OR CONCERNS OR WANT TO ACCESS MY PERSONAL DATA?</strong></p>

<p>Our Data Privacy Officer will be happy to answer any concerns or queries you may have relating to the use or storage of your personal data. You have the right to request access to, correction of and/or deletion of your data at any time by contacting our Data Privacy Officer. Our Data Privacy Officer can be contacted at data-protection@unity.com.</p>

<p>* &quot;Atanaya&quot;, &quot;Atanaya Hotel Bali&quot;, &quot;we&quot;, &quot;us&quot; and &quot;ourselves&quot; refers to all companies within the Atanaya International Limited corporate group.</p>

<p>Atanaya Hotel Bali; Sunset Road No. 88A, Kuta - Bali, Indonesia</p>        
   
  </div>
</section>
<!-- end of middle -->
<?php include('inc_footer.php');?>