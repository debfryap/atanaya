<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>WEBARQ - Static Website</title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
<link rel="icon" href="favicon.ico">
<!--Style-->
<link rel="stylesheet" href="css/reset.css">
<link rel="stylesheet" href="css/jquery-ui.css">
<link rel="stylesheet" href="css/nivo-slider.css">
<link rel="stylesheet" href="css/jquery.fancybox.css">
<link rel="stylesheet" href="css/style.css">
<link href="css/style_tablet.css" rel="stylesheet" type="text/css">
<link href="css/style_mobile.css" rel="stylesheet" type="text/css">
<!--js-->
<script src="js/vendor/jquery-1.9.1.min.js"></script>
<script>window.jQuery || document.write('<script src="js/vendor/jquery-1.9.1.min.js"><\/script>')</script>
<script src="js/plugins.js"></script>
<script src="js/vendor/modernizr-2.6.2.min.js"></script>
<script src="js/jquery-ui.js"></script>
<script src="js/jquery.nivo.slider.js"></script>
<script src="js/jquery.fancybox.js"></script>
<script src="js/TweenMax.js"></script>
<script src="js/jquery_function.js"></script>
</head>
<body>
<!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
            <![endif]--> 

<!-- Add your site or application content here --> 

<!-- header -->
<header class="afterclear">
  <div class="wrapper">
    <div class="logo"><a href="index.php"><img src="images/material/logo.png" alt=""></a></div>
    <div class="menu">
      <div class="top afterclear">
        <div class="phone"><a href="tel:03618468600">(+62 361) 8468600</a></div>
        <form action="" method="get">
          <div class="language">
            <div></div>
            <select name="">
              <option>English</option>
              <option>Indonesia</option>
            </select>
          </div>
        </form>
        <form action="" method="get" class="search">
          <input name="" type="text" placeholder="Search">
          <input name="" type="submit" value=" ">
        </form>
        <div class="lang_search">
          <div class="lang"><a href="#" class="active">ENG</a> | <a href="#">IND</a></div>
          <div class="search_mobile"><a href="#"><img src="images/material/icon-search-red.png" alt=""></a></div>
        </div>
        <a href="#" class="btn red" id="book-now">Book Now</a>
        <div id="menu-mobile">menu</div>
      </div>
      <nav class="main-menu">
        <ul>
       		<li><a href="index.php" <?php if($page == "page0"){echo"class=active";}?>>Home</a></li>
          <li><a href="offers.php" <?php if($page == "page1"){echo"class=active";}?>>special offers</a></li>
          <li><a href="activities.php" <?php if($page == "page2"){echo"class=active";}?>>activities</a></li>
          <li><a href="room.php" <?php if($page == "page3"){echo"class=active";}?>>Rooms</a>
            <ul>
              <li><a href="room-detail.php" class="active">DELUXE room</a></li>
              <li><a href="room-detail2.php">excecutive room</a></li>
              <li><a href="room-detail3.php">junior suite</a></li>
              <li><a href="room-detail4.php">excecutive suite</a></li>
            </ul>
          </li>
          <li><a href="restaurant.php" <?php if($page == "page4"){echo"class=active";}?>>Restaurant & Pool</a></li>
          <li><a href="meeting.php" <?php if($page == "page5"){echo"class=active";}?>>Meeting</a></li>
          <li><a href="contact.php" <?php if($page == "page6"){echo"class=active";}?>>Contact us</a></li>
        </ul>
        <div class="arrow-up"><a href="#"><img src="images/material/arrow-up.png" alt=""></a></div>
      </nav>
      <div id="search-mobile">
        <form action="" method="post">
          <input name="" type="text" placeholder="Search">
        </form>
      </div>
    </div>
  </div>
</header>
<!-- end of header -->