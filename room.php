<?php $page = "page3"; ?>
<?php include('inc_header.php');?>
<!-- middle -->
<div id="banner-content"><img src="images/slider/banner-rooms.jpg" alt=""></div>
<?php include("inc_booking.php"); ?>
<div id="breadcumb">
  <div class="wrapper"><a href="#">Home</a> / <a href="#">Room</a></div>
</div>
<section id="main-content" class="with-flower">
  <div class="wrapper">
    <h2 class="title">Room</h2>
    <p class="wording">kita memiliki 4 kamar berbeda dengan fasilitas kamar Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore  volutpat ipsum magna aliquam erat volutpat ipsum dolor sit amet, consectetuer .</p>
    <div class="double_line"></div>
    <ul class="list-room">
      <li><div class="images"><a href="room-detail.php"><img src="images/content/img-rooms.jpg" alt=""></a></div>
        <div class="box">
          <h4><a href="#">DELUXE room</a></h4>
        </div>
      </li>
      <li><div class="images"><a href="room-detail.php"><img src="images/content/img-rooms-10.jpg" alt=""></a></div>
        <div class="box">
          <h4><a href="#">excecutive room</a></h4>
        </div>
      </li>
      <li><div class="images"><a href="room-detail.php"><img src="images/content/img-rooms-12.jpg" alt=""></a></div>
        <div class="box">
          <h4><a href="#">junior suite</a></h4> 
        </div>
      </li>
      <li><div class="images"><a href="room-detail.php"><img src="images/content/img-rooms-14.jpg" alt=""></a></div>
        <div class="box">
          <h4><a href="#">excecutive room</a></h4>
        </div>
      </li>
    </ul>
  </div>
</section>

<!-- end of middle -->
<?php include('inc_footer.php');?>