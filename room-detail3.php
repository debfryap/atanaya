<?php $page = "page3"; ?>
<?php include('inc_header.php');?>
<script src="js/jquery.carouFredSel-6.2.1-packed.js"></script> 
<!-- middle -->
<div id="banner-content"><img src="images/slider/banner-offer.jpg" alt=""></div>
<?php include("inc_booking.php"); ?>
<div id="breadcumb">
  <div class="wrapper"><a href="#">Home</a> / <a href="#">Rooms</a></div>
</div>
<section id="main-content" class="with-flower">
  <div class="wrapper">
    <div class="title-detail">junior suite
      <nav class="socmed"> <a href="#"><img src="images/material/icon-socmed.png" width="32" height="32" alt=""></a> <a href="#"><img src="images/material/icon-socmed-02.png" width="32" height="32" alt=""></a> <a href="#"><img src="images/material/icon-socmed-03.png" width="32" height="32" alt=""></a> <a href="#"><img src="images/material/icon-socmed-04.png" width="32" height="32" alt=""></a> </nav>
    </div>
    <div id="dragable">
      <div class="box"><img src="images/content/img-kapur.jpg" alt=""></div>
      <div class="box"><img src="images/content/img-content-room.jpg" alt=""></div>
      <div class="box"><img src="images/content/img-kapur.jpg" alt=""></div>
      <div class="box"><img src="images/content/img-content-room.jpg" alt=""></div>
    </div>
    
<p>Expressive of traditional peranakan décor, the deluxe hotel rooms at The Atanaya are the ideal location for settling into while exploring everything Bali has to offer. The modernized hotel accommodations offered at The Atanaya envelope guests with soothing colors, layout and welcoming amenities. Each of the Deluxe Rooms features complimentary daily breakfast for two, and tea and coffee making facilities. </p>
<p>Features</p>
<ul class="col-list">
  <li>23 sqm</li>
  <li> Queen/Twin Bed</li>
  <li> 32” flat screen TV</li>
  <li> Shower</li>
  <li> Complimentary WiFi</li>
  <li> Work space/Desk</li>
  <li> In-room safe</li>
  <li> Tea &amp; coffee making facilities</li>
  <li> Daily Buffet Breakfast for 2<br />
  </li>
</ul>
<div class="call">
      <h5>Are you interested?</h5>      
      <div class="right"><a href="#" class="btn line">CALL  (+62 361) 8468600</a> <span>OR</span> <a href="#" class="btn red">MAKE RESERVATION</a></div>
    </div>
    <div class="double_line" style="margin-top:25px;"></div>
    <nav class="prevnext afterclear"><a href="#">PREVIOUS ROOM</a> <a href="#">NEXT ROOM</a></nav>   
    <div class="list_carousel">
      <h5>OTHER PROMOTION</h5>
      <ul class="list-image afterclear" id="foo2">
      <li>
        <div class="images"><img src="images/content/img-offer.jpg" alt="LIMITED TIME OFFER" >
          
        </div>
        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy .</p>
      </li>
      <li>
        <div class="images"><img src="images/content/img-offer-05.jpg" alt="2 Nights Stay Special Deal" >
          
        </div>
        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy .</p>
      </li>
      <li>
        <div class="images"><img src="images/content/img-offer-07.jpg" alt="Honeymoon Package" >
          
        </div>
        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy .</p>
      </li>
      <li>
        <div class="images"><img src="images/content/img-offer-09.jpg" alt="Meeting Packages" >
          
        </div>
        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy .</p>
      </li>
      <li>
        <div class="images"><img src="images/content/img-offer.jpg" alt="LIMITED TIME OFFER" >
          
        </div>
        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy .</p>
      </li>
      <li>
        <div class="images"><img src="images/content/img-offer-05.jpg" alt="2 Nights Stay Special Deal" >
          
        </div>
        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy .</p>
      </li>
      <li>
        <div class="images"><img src="images/content/img-offer-07.jpg" alt="Honeymoon Package" >
          
        </div>
        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy .</p>
      </li>
      <li>
        <div class="images"><img src="images/content/img-offer-09.jpg" alt="Meeting Packages" >
          
        </div>
        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy .</p>
      </li>
    </ul>
      <div class="clearfix"></div>
      <a id="prev2" class="prev" href="#">&lt;</a> <a id="next2" class="next" href="#">&gt;</a>
      <div id="pager2" class="pager"></div>
    </div> 
  </div>
</section>
<script src="http://cdnjs.cloudflare.com/ajax/libs/gsap/1.15.1/utils/Draggable.min.js"></script> 
<script src="http://cdnjs.cloudflare.com/ajax/libs/gsap/1.15.1/plugins/CSSPlugin.min.js"></script> 
<script src="js/ThrowPropsPlugin.min.js"></script> 
<script src="js/slider-dragable.js"></script> 
<script type="text/javascript">
	$(window).load(function(e) {
        $('#foo2').carouFredSel({
		  auto: false,
		  prev: '#prev2',
		  next: '#next2',
		  mousewheel: true,
		  swipe: {
			  onMouse: true,
			  onTouch: true
		  }
	  });
	  $("#dragable").slider_drag();
    });
</script> 
<!-- end of middle -->
<?php include('inc_footer.php');?>